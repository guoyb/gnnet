import numpy as np
import random as rn
# fix random seeds to reduce variability of results
np.random.seed(2020)
rn.seed(2020)
import tensorflow as tf
from keras import backend as K
tf.set_random_seed(2020)
import numpy
from keras.layers import *
import keras
from keras.engine.topology import Layer
from keras import initializers
import warnings
import DataRep
from keras import initializers
from keras.models import Model
from keras.optimizers import Adam,RMSprop,Adagrad,Adadelta,RAdam,AdaX,Nadam
from keras.layers.noise import *
from keras.regularizers import l2
from keras.layers.embeddings import Embedding
from keras import backend as K
from keras.initializers import Constant
from keras import initializers
seed = 2020
L2_value=0.0001
def DGCN_gelu(seq,kernel_size,dropout_rate):
    dim = K.int_shape(seq)[-1]
    h = Conv1D(dim*2, kernel_size, padding='same',kernel_regularizer=l2(L2_value),kernel_initializer = initializers.glorot_uniform(seed = seed))(seq)
    def _gate(x):
        s, h = x
        g, h = h[:, :, :dim], h[:, :, dim:]
        g = K.sigmoid(g)
        h = K.gelu(h)
        s = K.dropout(s, dropout_rate)
        return s + g * h
    seq = Lambda(_gate)([seq, h])
    return seq

def DGCRN_gelu(filters, ks, t_input, dropout_rate):
    ini_featuresb0 = DGCN_gelu(t_input, ks,dropout_rate) 
    ini_featuresb1 = DGCN_gelu(t_input, ks+6,dropout_rate) 
    ini_featuresb2 = DGCN_gelu(t_input, ks+12,dropout_rate) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2])
    g_features = Conv1D(filters,3,activation='gelu',padding='same',kernel_regularizer=l2(L2_value),kernel_initializer = initializers.glorot_uniform(seed = seed))(ini_featuresb)
    return g_features 
#############################################################################################################################################
def build_GNNeta(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix,lr, ks, units):
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    c1 = Conv1D(filters,3,activation='gelu')(embed_features)
    cfeatures = DGCRN_gelu(filters, ks, c1, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures])
    mfeatures = MaxPooling1D(3)(cfeatures)
    afeatures = Dropout(dropout_rate)(mfeatures)
    f_fetures = GRU(return_sequences=True, units = filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    b_fetures = GRU(return_sequences=True, go_backwards=True, units=filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    a_features = keras.layers.add([f_fetures,b_fetures])
    all_features = keras.layers.add([a_features,afeatures]) 
    rnn_features = Dropout(dropout_rate+0.5)(all_features)
    features = Flatten()(rnn_features)
    output = Dense(nb_classes,activation = 'softmax')(features)
    GNNeta = Model(inputs=[input_sequences], outputs=[output])
    opt =  Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    GNNeta.compile(optimizer= opt, loss='binary_crossentropy', metrics=['accuracy']) 
    GNNeta.summary()
    GNNeta.get_config()
    return GNNeta

############################################################################
def build_GNNetb(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix,lr, ks, units):
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    c1 = Conv1D(filters,3,activation='gelu')(embed_features)
    cfeatures = DGCRN_gelu(filters, ks, c1, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures])
    mfeatures = MaxPooling1D(3)(cfeatures)
    afeatures = Dropout(dropout_rate)(mfeatures)
    f_fetures = GRU(return_sequences=True, units = filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    all_features = keras.layers.add([f_fetures,afeatures])
    rnn_features = Dropout(dropout_rate+0.5)(all_features)
    features = Flatten()(rnn_features)
    output = Dense(nb_classes,activation = 'softmax')(features)
    GNNetb = Model(inputs=[input_sequences], outputs=[output])
    opt =  Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    GNNetb.compile(optimizer= opt, loss='binary_crossentropy', metrics=['accuracy']) 
    GNNetb.summary()
    GNNetb.get_config()
    return GNNetb

def build_GNNetc(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix,lr, ks, units):
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    c1 = Conv1D(filters,3,activation='gelu')(embed_features)
    cfeatures = DGCRN_gelu(filters, ks, c1, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures])
    mfeatures = MaxPooling1D(3)(cfeatures)
    afeatures = Dropout(dropout_rate)(mfeatures)
    b_fetures = GRU(return_sequences=True, go_backwards=True, units=filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    all_features = keras.layers.add([b_fetures,afeatures])
    rnn_features = Dropout(dropout_rate+0.5)(all_features)
    features = Flatten()(rnn_features)
    output = Dense(nb_classes,activation = 'softmax')(features)
    GNNetc = Model(inputs=[input_sequences], outputs=[output])
    opt =  Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    GNNetc.compile(optimizer= opt, loss='binary_crossentropy', metrics=['accuracy']) 
    GNNetc.summary()
    GNNetc.get_config()
    return GNNetc

##########################################################################################################

def build_GNNetonehot(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix,lr, ks, units):
    embedding_layer = Embedding(len(word_index) + 1,
                        len(word_index),
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    c1 = Conv1D(filters,3,activation='gelu')(embed_features)
    cfeatures = DGCRN_gelu(filters, ks, c1, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures])
    mfeatures = MaxPooling1D(3)(cfeatures)
    afeatures = Dropout(dropout_rate)(mfeatures)
    f_fetures = GRU(return_sequences=True, units = filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    b_fetures = GRU(return_sequences=True, go_backwards=True, units=filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    a_features = keras.layers.add([f_fetures,b_fetures])
    all_features = keras.layers.add([a_features,afeatures])  
    rnn_features = Dropout(dropout_rate+0.5)(all_features)
    features = Flatten()(rnn_features)
    output = Dense(nb_classes,activation = 'softmax')(features)
    GNNetonehot = Model(inputs=[input_sequences], outputs=[output])
    opt =  Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    GNNetonehot.compile(optimizer= opt, loss='binary_crossentropy', metrics=['accuracy']) 
    GNNetonehot.summary()
    GNNetonehot.get_config()
    return GNNetonehot

def build_GNNetrand(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix,lr, ks, units):
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    c1 = Conv1D(filters,3,activation='gelu')(embed_features)
    cfeatures = DGCRN_gelu(filters, ks, c1, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_gelu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures])
    mfeatures = MaxPooling1D(3)(cfeatures)
    afeatures = Dropout(dropout_rate)(mfeatures)
    f_fetures = GRU(return_sequences=True, units = filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    b_fetures = GRU(return_sequences=True, go_backwards=True, units=filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    a_features = keras.layers.add([f_fetures,b_fetures])
    all_features = keras.layers.add([a_features,afeatures])
    rnn_features = Dropout(dropout_rate+0.5)(all_features)
    features = Flatten()(rnn_features)
    # fc1 = Dense(units, activation='gelu',kernel_initializer = initializers.glorot_uniform(seed = seed))(features)
    output = Dense(nb_classes,activation = 'softmax')(features)
    GNNetrand = Model(inputs=[input_sequences], outputs=[output])
    opt =  Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    GNNetrand.compile(optimizer= opt, loss='binary_crossentropy', metrics=['accuracy']) 
    GNNetrand.summary()
    GNNetrand.get_config()
    return GNNetrand
####################>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
def DGCN_linear(seq,kernel_size,dropout_rate):
    dim = K.int_shape(seq)[-1]
    h = Conv1D(dim*2, kernel_size, padding='same',kernel_regularizer=l2(L2_value),kernel_initializer = initializers.glorot_uniform(seed = seed))(seq)
    def _gate(x):
        s, h = x
        g, h = h[:, :, :dim], h[:, :, dim:]
        g = K.sigmoid(g)
        h = K.linear(h)
        s = K.dropout(s, dropout_rate)        
        return s+g * h
    seq = Lambda(_gate)([seq, h])
    return seq
def DGCRN_linear(filters, ks, t_input, dropout_rate):
    ini_featuresb0 = DGCN_linear(t_input, ks,dropout_rate) 
    ini_featuresb1 = DGCN_linear(t_input, ks+6,dropout_rate) 
    ini_featuresb2 = DGCN_linear(t_input, ks+12,dropout_rate) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2])
    g_features = Conv1D(filters,3,activation='gelu',padding='same',kernel_regularizer=l2(L2_value),kernel_initializer = initializers.glorot_uniform(seed = seed))(ini_featuresb)
    return g_features 

def build_GNNet_linear(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix,lr, ks, units):
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    c1 = Conv1D(filters,3,activation='gelu')(embed_features)
    cfeatures = DGCRN_linear(filters, ks, c1, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_linear(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_linear(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures])
    mfeatures = MaxPooling1D(3)(cfeatures)
    afeatures = Dropout(dropout_rate)(mfeatures)
    f_fetures = GRU(return_sequences=True, units = filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    b_fetures = GRU(return_sequences=True, go_backwards=True, units=filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    a_features = keras.layers.add([f_fetures,b_fetures])
    all_features = keras.layers.add([a_features,afeatures])
    rnn_features = Dropout(dropout_rate+0.5)(all_features)
    features = Flatten()(rnn_features)
    output = Dense(nb_classes,activation = 'softmax')(features)
    GNNet_linear = Model(inputs=[input_sequences], outputs=[output])
    opt =  Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    GNNet_linear.compile(optimizer= opt, loss='binary_crossentropy', metrics=['accuracy']) 
    GNNet_linear.summary()
    GNNet_linear.get_config()
    return GNNet_linear
####################>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

def DGCN_relu(seq,kernel_size,dropout_rate):
    dim = K.int_shape(seq)[-1]
    h = Conv1D(dim*2, kernel_size, padding='same',kernel_regularizer=l2(L2_value),kernel_initializer = initializers.glorot_uniform(seed = seed))(seq)
    def _gate(x):
        s, h = x
        g, h = h[:, :, :dim], h[:, :, dim:]
        g = K.sigmoid(g)
        h = K.relu(h)
        s = K.dropout(s, dropout_rate)
        return s+g * h
    seq = Lambda(_gate)([seq, h])
    return seq
def DGCRN_relu(filters, ks, t_input, dropout_rate):
    ini_featuresb0 = DGCN_relu(t_input, ks,dropout_rate) 
    ini_featuresb1 = DGCN_relu(t_input, ks+6,dropout_rate) 
    ini_featuresb2 = DGCN_relu(t_input, ks+12,dropout_rate) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2])
    g_features = Conv1D(filters,3,activation='gelu',padding='same',kernel_regularizer=l2(L2_value),kernel_initializer = initializers.glorot_uniform(seed = seed))(ini_featuresb)
    return g_features 

def build_GNNet_relu(args, word_index, nb_classes, dropout_rate, filters, embedding_matrix,lr, ks, units):
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    c1 = Conv1D(filters,3,activation='gelu')(embed_features)
    cfeatures = DGCRN_relu(filters, ks, c1, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_relu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures]) 
    cfeatures = DGCRN_relu(filters, ks, cfeatures, dropout_rate)
    cfeatures = keras.layers.add([c1,cfeatures])
    mfeatures = MaxPooling1D(3)(cfeatures)
    afeatures = Dropout(dropout_rate)(mfeatures)
    f_fetures = GRU(return_sequences=True, units = filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    b_fetures = GRU(return_sequences=True, go_backwards=True, units=filters,activation='tanh',recurrent_activation='sigmoid')(afeatures)
    a_features = keras.layers.add([f_fetures,b_fetures])
    all_features = keras.layers.add([a_features,afeatures])
    rnn_features = Dropout(dropout_rate+0.5)(all_features)
    features = Flatten()(rnn_features)
    output = Dense(nb_classes,activation = 'softmax')(features)
    GNNet_relu = Model(inputs=[input_sequences], outputs=[output])
    opt =  Adam(lr=lr, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    GNNet_relu.compile(optimizer= opt, loss='binary_crossentropy', metrics=['accuracy']) 
    GNNet_relu.summary()
    GNNet_relu.get_config()
    return GNNet_relu
